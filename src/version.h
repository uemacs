#ifndef VERSION_H
#define VERSION_H

#define PROGRAM_NAME "uem"
#define PROGRAM_NAME_LONG "uemacs"
#define	VERSION	"4.0.15.1"

/* Print the version string. */
void version(void);

#endif /* VERSION_H */
