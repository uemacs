#include <stdio.h>

#include "version.h"

void version(void)
{
	printf("%s version %s\n", PROGRAM_NAME_LONG, VERSION);
	printf("Derived from uEmacs/PK 4.0.15\n");
}
