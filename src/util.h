#ifndef UTIL_H_
#define UTIL_H_

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

extern size_t strlcpy(char *dest, const char *src, size_t size);
extern void *xmalloc(size_t size);
extern void *xzmalloc(size_t size);
extern void *xmalloc_aligned(size_t size, size_t alignment);
extern void *xmallocz(size_t size);
extern void *xmemdupz(const void *data, size_t len);
extern void *xcalloc(size_t nmemb, size_t size);
extern void *xrealloc(void *ptr, size_t nmemb, size_t size);
extern void xfree(void *ptr);

#endif  /* UTIL_H_ */
