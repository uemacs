#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include "util.h"

#ifndef SIZE_T_MAX
# define SIZE_T_MAX  ((size_t) ~0)
#endif

void *xmalloc(size_t size)
{
	void *ptr;

	if (size == 0) {
		fprintf(stderr, "xmalloc: zero size\n");
		exit(EXIT_FAILURE);
	}

	ptr = malloc(size);
	if (ptr == NULL) {
		fprintf(stderr, "xmalloc: out of memory "
				"(allocating %lu bytes)\n",
				(u_long) size);
		exit(EXIT_FAILURE);
	}

	return ptr;
}

void *xzmalloc(size_t size)
{
	void *ptr;

	if (size == 0) {
		fprintf(stderr, "xzmalloc: zero size\n");
		exit(EXIT_FAILURE);
	}

	ptr = malloc(size);
	if (ptr == NULL) {
		fprintf(stderr, "xzmalloc: out of memory "
				"(allocating %lu bytes)\n",
				(u_long) size);
		exit(EXIT_FAILURE);
	}

	memset(ptr, 0, size);

	return ptr;
}

void *xmalloc_aligned(size_t size, size_t alignment)
{
	int ret;
	void *ptr;

	if (size == 0) {
		fprintf(stderr, "xmalloc_aligned: zero size\n");
		exit(EXIT_FAILURE);
	}

	ret = posix_memalign(&ptr, alignment, size);
	if (ret != 0) {
		fprintf(stderr, "xmalloc_aligned: out of memory "
				"(allocating %lu bytes)\n",
				(u_long) size);
		exit(EXIT_FAILURE);
	}

	return ptr;
}

void *xmallocz(size_t size)
{
	void *ptr;

	if (size + 1 < size) {
		fprintf(stderr, "xmallocz: data too large to fit "
				"into virtual memory space\n");
		exit(EXIT_FAILURE);
	}

	ptr = xmalloc(size + 1);
	((char*) ptr)[size] = 0;

	return ptr;
}

void *xmemdupz(const void *data, size_t len)
{
	return memcpy(xmallocz(len), data, len);
}

void *xcalloc(size_t nmemb, size_t size)
{
	void *ptr;

	if (size == 0 || nmemb == 0) {
		fprintf(stderr, "xcalloc: zero size\n");
		exit(EXIT_FAILURE);
	}
	if (SIZE_T_MAX / nmemb < size) {
		fprintf(stderr, "xcalloc: nmemb * size > SIZE_T_MAX\n");
		exit(EXIT_FAILURE);
	}

	ptr = calloc(nmemb, size);
	if (ptr == NULL) {
		fprintf(stderr, "xcalloc: out of memory "
				"(allocating %lu bytes)\n",
				(u_long) (size * nmemb));
		exit(EXIT_FAILURE);
	}

	return ptr;
}

void *xrealloc(void *ptr, size_t nmemb, size_t size)
{
	void *new_ptr;
	size_t new_size = nmemb * size;

	if (new_size == 0) {
		fprintf(stderr, "xrealloc: zero size\n");
		exit(EXIT_FAILURE);
	}
	if (SIZE_T_MAX / nmemb < size) {
		fprintf(stderr, "xrealloc: nmemb * size > "
				"SIZE_T_MAX\n");
		exit(EXIT_FAILURE);
	}

	if (ptr == NULL)
		new_ptr = malloc(new_size);
	else
		new_ptr = realloc(ptr, new_size);

	if (new_ptr == NULL) {
		fprintf(stderr, "xrealloc: out of memory "
				"(new_size %lu bytes)\n",
				(u_long) new_size);
		exit(EXIT_FAILURE);
	}

	return new_ptr;
}

void xfree(void *ptr)
{
	if (ptr == NULL) {
		fprintf(stderr, "xfree: NULL pointer given as argument\n");
		exit(EXIT_FAILURE);
	}

	free(ptr);
}
